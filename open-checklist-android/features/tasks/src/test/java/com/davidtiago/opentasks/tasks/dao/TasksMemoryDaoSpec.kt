package com.davidtiago.opentasks.tasks.dao

import com.davidtiago.opentasks.tasks.data.Task
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import kotlin.test.assertEquals

object TasksMemoryDaoSpec : Spek({
    val memoryDao by memoized { TasksMemoryDao() }
    describe("initialization") {
        it("returns empty when nothing added") {
            assertEquals(
                expected = memoryDao.getTasks(),
                actual = listOf()
            )
        }
    }
    describe("saveTask") {
        describe("adding single item") {
            val taskToSave = Task("Task description")
            beforeEachTest {
                memoryDao.saveTask(taskToSave)
            }
            it("should return single item") {
                assertEquals(
                    expected = memoryDao.getTasks().size,
                    actual = 1
                )
            }
            it("should return saved task") {
                assertEquals(
                    expected = memoryDao.getTasks()[0],
                    actual = taskToSave
                )
            }
        }
        describe("adding multiple items") {
            val taskOneToSave = Task("Task 1 description")
            val taskTwoToSave = Task("Task 2 description")
            beforeEachTest {
                memoryDao.saveTask(taskOneToSave)
                memoryDao.saveTask(taskTwoToSave)
            }
            it("should return both tasks") {
                assertEquals(
                    expected = memoryDao.getTasks().size,
                    actual = 2
                )
            }
            it("should return right first task") {
                assertEquals(
                    expected = memoryDao.getTasks()[0],
                    actual = taskOneToSave
                )
            }
            it("should return right second task") {
                assertEquals(
                    expected = memoryDao.getTasks()[1],
                    actual = taskTwoToSave
                )
            }
        }
    }
})
