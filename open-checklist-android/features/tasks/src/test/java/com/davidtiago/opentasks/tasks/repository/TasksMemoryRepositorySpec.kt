package com.davidtiago.opentasks.tasks.repository

import com.davidtiago.opentasks.tasks.dao.TasksDao
import com.davidtiago.opentasks.tasks.data.Task
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import kotlin.test.assertEquals

val FILLED_TASK_LIST = mutableListOf(
    Task("First task"),
    Task("Second task")
)

object TasksMemoryRepositorySpec : Spek({
    val fakeDao by memoized { TasksDaoFake() }
    val tasksRepository by memoized { TasksMemoryRepository(fakeDao) }
    describe("load") {
        describe("loads from dao") {
            lateinit var loadedTasks: List<Task>
            beforeEachTest {
                fakeDao.taskList = FILLED_TASK_LIST
                loadedTasks = tasksRepository.loadTasks()
            }
            it("should invoke dao once") {
                assertEquals(
                    actual = fakeDao.getTasksCount,
                    expected = 1
                )
            }
            it("should return same size") {
                assertEquals(
                    actual = loadedTasks.size,
                    expected = 2
                )
            }
            it("should return correct first item") {
                assertEquals(
                    actual = loadedTasks[0],
                    expected = FILLED_TASK_LIST[0]
                )
            }
            it("should return second item") {
                assertEquals(
                    actual = loadedTasks[1],
                    expected = FILLED_TASK_LIST[1]
                )
            }
        }
    }
    describe("save") {
        describe("saves to empty dao") {
            val newTaskDescription = "new task to save"
            beforeEachTest {
                tasksRepository.save(
                    Task(newTaskDescription)
                )
            }
            it("should invoke dao once") {
                assertEquals(
                    actual = fakeDao.saveTasksCount,
                    expected = 1
                )
            }
            it("should increase count") {
                assertEquals(
                    actual = tasksRepository.loadTasks().count(),
                    expected = 1
                )
            }
            it("should have the right first element") {
                assertEquals(
                    actual = tasksRepository.loadTasks().first(),
                    expected = Task(newTaskDescription)
                )
            }
        }
        describe("saves to filled dao") {
            val newTaskDescription = "new task to save"
            beforeEachTest {
                fakeDao.taskList = FILLED_TASK_LIST
                tasksRepository.save(
                    Task(newTaskDescription)
                )
            }
            it("should increase count") {
                assertEquals(
                    actual = tasksRepository.loadTasks().count(),
                    expected = 3
                )
            }
            it("should contain added element") {
                assertEquals(
                    actual = tasksRepository.loadTasks()
                        .contains(
                            Task(newTaskDescription)
                        ),
                    expected = true
                )
            }
        }
    }
})

class TasksDaoFake : TasksDao {
    var taskList: MutableList<Task> = mutableListOf()
    var getTasksCount = 0
    var saveTasksCount = 0

    override fun getTasks(): List<Task> {
        getTasksCount++
        return taskList
    }

    override fun saveTask(task: Task) {
        saveTasksCount++
        taskList.add(task)
    }
}
