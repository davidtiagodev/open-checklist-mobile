package com.davidtiago.opentasks.tasks.addtask

import com.davidtiago.core_tests.makeArchTaskExecutorInstant
import com.davidtiago.core_tests.resetArchTaskExecutor
import com.davidtiago.opentasks.tasks.data.Task
import com.davidtiago.opentasks.tasks.repository.TasksRepository
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import kotlin.test.assertEquals

object AddViewModelSpec : Spek({
    val tasksRepositoryFake by memoized { FakeTaskRepository() }
    val addViewModel by memoized { AddViewModel(tasksRepositoryFake) }
    beforeEachTest {
        makeArchTaskExecutorInstant()
    }
    afterEachTest {
        resetArchTaskExecutor()
    }
    describe("initialization") {
        it("should start with enabled state") {
            assertEquals(
                expected = AddState.Enabled,
                actual = addViewModel.state.value
            )
        }
    }

    describe("save") {
        val description = "Task Description"
        beforeEachTest {
            addViewModel.save(description)
        }
        it("should invoke repository") {
            assertEquals(
                expected = tasksRepositoryFake.savedTask,
                actual = Task(description)
            )
        }
        it("should change state after save") {
            assertEquals(
                expected = AddState.Saved,
                actual = addViewModel.state.value
            )
        }
    }
    describe("State changes") {
        val states = mutableListOf<AddState>()
        beforeEachTest {
            addViewModel.state.observeForever { states.add(it) }
            addViewModel.save("Some description")
        }
        it("should states in the right order") {
            assertEquals(
                expected = listOf(AddState.Enabled, AddState.Saving, AddState.Saved),
                actual = states
            )
        }
    }
})

private class FakeTaskRepository : TasksRepository {
    var savedTask: Task? = null
    override fun loadTasks(): List<Task> {
        TODO("Not yet implemented")
    }

    override fun save(task: Task) {
        savedTask?.let { throw IllegalStateException("Save invoked multiple times") }
        savedTask = task
    }
}
