package com.davidtiago.opentasks.tasks.repository

import com.davidtiago.opentasks.tasks.dao.TasksDao
import com.davidtiago.opentasks.tasks.data.Task

interface TasksRepository {
    fun loadTasks(): List<Task>
    fun save(task: Task)
}

class TasksMemoryRepository(private val tasksDao: TasksDao) : TasksRepository {
    override fun loadTasks(): List<Task> {
        return tasksDao.getTasks()
    }

    override fun save(task: Task): Unit = tasksDao.saveTask(task)
}
