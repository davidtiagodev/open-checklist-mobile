package com.davidtiago.opentasks.tasks.data

data class Task(val description: String)
