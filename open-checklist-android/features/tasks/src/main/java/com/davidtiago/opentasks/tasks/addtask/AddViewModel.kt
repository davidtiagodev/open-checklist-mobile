package com.davidtiago.opentasks.tasks.addtask

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.davidtiago.opentasks.tasks.data.Task
import com.davidtiago.opentasks.tasks.repository.TasksRepository

class AddViewModel(
    private val tasksRepository: TasksRepository
) : ViewModel() {

    private val _state: MutableLiveData<AddState> by lazy {
        MutableLiveData<AddState>(AddState.Enabled)
    }
    val state: LiveData<AddState>
        get() = _state

    fun save(description: String) {
        _state.value = AddState.Saving
        tasksRepository.save(Task(description))
        _state.value = AddState.Saved
    }
}

sealed class AddState {
    object Enabled : AddState()
    object Saving : AddState()
    object Saved : AddState()
}
