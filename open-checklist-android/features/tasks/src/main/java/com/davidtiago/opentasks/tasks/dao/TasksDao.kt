package com.davidtiago.opentasks.tasks.dao

import com.davidtiago.opentasks.tasks.data.Task

interface TasksDao {
    fun getTasks(): List<Task>
    fun saveTask(task: Task)
}
