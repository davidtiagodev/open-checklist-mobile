package com.davidtiago.opentasks.tasks.dao

import com.davidtiago.opentasks.tasks.data.Task

class TasksMemoryDao : TasksDao {

    private val tasks: MutableList<Task> = mutableListOf()

    override fun getTasks(): List<Task> = tasks

    override fun saveTask(task: Task) {
        tasks.add(task)
    }
}
