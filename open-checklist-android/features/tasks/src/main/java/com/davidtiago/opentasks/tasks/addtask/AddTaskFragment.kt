package com.davidtiago.opentasks.tasks.addtask

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.davidtiago.opentasks.tasks.databinding.FragmentAddTaskBinding

class AddTaskFragment : Fragment() {

    lateinit var addViewModel: AddViewModel

    private var _binding: FragmentAddTaskBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAddTaskBinding.inflate(
            inflater,
            container,
            false
        )
        setupViews()
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupViews() {
        with(binding) {
            save.setOnClickListener {
                addViewModel.save(edit.text.toString())
            }
        }
    }

}
