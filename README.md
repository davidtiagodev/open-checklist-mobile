# open-checklist-mobile

The development of this project was stopped since I moved my focus to [android-splash])(https://gitlab.com/davidtiagoconceicao/android-splash).

Mobile checklist app for study and experiments.

This project is heavily inspired by the [official guidelines](https://developer.android.com/jetpack/docs/guide) for Android application architectures. An evolutionary approach was chosen, with components, dependencies and everything else incorporated only when required.
All the project documentation can be found under docs directory, distributed under specific files. The sections below are some are highlights of some aspects.

## Modules
This is project uses an modular approach from the start, avoiding later refactor. The feature modules are created under `:features` while the library modules will be created under `:libraries`. An feature is an starting point of any user flow, as suggested by [Jeroen Mols](https://jeroenmols.com/blog/2019/03/18/modularizationarchitecture/). Any module with component used by other features will be considered a library.

## Tests
One of the main goals of this project is to expand the Android testing basics, experimenting with different approaches, techniques and tools. They are detailed at [tests.md](docs/tests.md) doc.

## Continuous integration
This app is developed using [trunk based development](trunkbaseddevelopment.com/) with a pipeline responsible for checking the master branch state. More about the pipeline in [this doc](docs/pipeline.md).
